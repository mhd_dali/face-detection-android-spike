package com.photobook.facedetection

import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.vision.Frame
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetector
import com.google.mlkit.vision.face.FaceDetectorOptions
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.atomic.AtomicBoolean
import com.google.android.gms.vision.face.FaceDetector as VisionFaceDetector


class MainActivity : AppCompatActivity() {

    val TAG = MainActivity::class.java.canonicalName
    lateinit var myRectPaint: Paint
    val options = BitmapFactory.Options()
    lateinit var myBitmap: Bitmap

    lateinit var faceDetector: VisionFaceDetector

    var isProcessing = AtomicBoolean(false)

    lateinit var highAccuracyOpts: FaceDetectorOptions
    lateinit var mlKitImage: InputImage
    lateinit var mlKitDetector: FaceDetector
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        options.inMutable = true
        myBitmap = BitmapFactory.decodeResource(resources, R.drawable.group1, options)

        myRectPaint = Paint()
        myRectPaint.strokeWidth = 5f
        myRectPaint.color = Color.BLUE
        myRectPaint.style = Paint.Style.STROKE

        highAccuracyOpts = FaceDetectorOptions.Builder()
            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_FAST)
            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_NONE)
            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_NONE)
            .build()

        buttonVision.setOnClickListener {
            processWithVision()
        }

        buttonMlKit.setOnClickListener {
            processMLKitFaceDetector()
        }
    }

    fun processWithVision() {
        Thread {
            Log.d(TAG, "initing FaceDetector")
            val initTime = System.currentTimeMillis()
            faceDetector =
                VisionFaceDetector.Builder(applicationContext).setTrackingEnabled(false).build()
            Log.d(TAG, "done initing FaceDetector")

            when {
                !faceDetector.isOperational -> {
                    Toast.makeText(
                        this@MainActivity,
                        "Could not set up the face detector!",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }
                isProcessing.get() -> {
                    Toast.makeText(
                        this@MainActivity,
                        "Detecting faces, please wait",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    val tempBitmap =
                        Bitmap.createBitmap(myBitmap.width, myBitmap.height, Bitmap.Config.RGB_565)
                    val tempCanvas = Canvas(tempBitmap)
                    tempCanvas.drawBitmap(myBitmap, 0f, 0f, null)

                    Log.d(TAG, "initing frame")
                    val frame = Frame.Builder().setBitmap(myBitmap).build()
                    isProcessing.set(true)
                    Log.d(TAG, "detecting faces")
                    val startTime = System.currentTimeMillis()
                    val faces = faceDetector.detect(frame) // takes time
                    val endTime = System.currentTimeMillis()

                    Log.d(TAG, "done detecting faces")
                    for (i in 0 until faces.size()) {
                        val face = faces.valueAt(i)
                        face?.let {
                            tempCanvas.drawRect(
                                RectF(
                                    face.position.x,
                                    face.position.y,
                                    face.position.x + face.width,
                                    face.position.y + face.height
                                ),
                                myRectPaint
                            )
                        }
                    }
                    Log.d(TAG, "done drawing rects for faces")
                    val resultBitmap = BitmapDrawable(resources, tempBitmap)
                    runOnUiThread {
                        fillTextData(
                            "Google Vision",
                            startTime - initTime,
                            endTime - startTime,
                            faces.size()
                        )
                        Log.d(TAG, "drawing the image")
                        imgview.setImageDrawable(resultBitmap)
                    }
                    Log.d(TAG, "Done!")
                    isProcessing.set(false)
                }
            }
        }.start()
    }

    fun processMLKitFaceDetector() {
        // High-accuracy landmark detection and face classification
        // High-accuracy landmark detection and face classification
        Log.d(TAG, "initing FaceDetector")

        when {
            isProcessing.get() -> {
                Toast.makeText(
                    this@MainActivity,
                    "Detecting faces, please wait",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {
                Thread {
                    isProcessing.set(true)
                    val initTime = System.currentTimeMillis()
                    mlKitImage = InputImage.fromBitmap(myBitmap, 0)

                    mlKitDetector = FaceDetection.getClient(highAccuracyOpts)
                    val tempBitmap =
                        Bitmap.createBitmap(myBitmap.width, myBitmap.height, Bitmap.Config.RGB_565)
                    val tempCanvas = Canvas(tempBitmap)
                    tempCanvas.drawBitmap(myBitmap, 0f, 0f, null)
                    Log.d(TAG, "detecting faces")
                    val startTime = System.currentTimeMillis()
                    mlKitDetector.process(mlKitImage)
                        .addOnSuccessListener { faces ->
                            Log.d(TAG, "done detecting faces")
                            val endTime = System.currentTimeMillis()
                            fillTextData(
                                "ML Kit",
                                startTime - initTime,
                                endTime - startTime,
                                faces.size
                            )

                            for (face in faces) {
                                tempCanvas.drawRect(
                                    Rect(
                                        face.boundingBox.left,
                                        face.boundingBox.top,
                                        face.boundingBox.right,
                                        face.boundingBox.bottom
                                    ),
                                    myRectPaint
                                )
                            }
                            val resultBitmap = BitmapDrawable(resources, tempBitmap)
                            runOnUiThread {
                                Log.d(TAG, "drawing the image")
                                imgview.setImageDrawable(resultBitmap)
                            }
                            Log.d(TAG, "Done!")
                            isProcessing.set(false)

                        }
                        .addOnFailureListener { e ->
                            e.printStackTrace()
                            isProcessing.set(false)
                        }
                }.start()
            }
        }
    }

    fun fillTextData(
        detectionType: String,
        initDuration: Long,
        duration: Long,
        detectedFacesCount: Int
    ) {
        val durationInSeconds = duration.toFloat() / 1000f
        durationTextView.text = "Current Duration: $durationInSeconds"
        val initDurationInSeconds = initDuration.toFloat() / 1000f
        imageSizeTextView.text = "${myBitmap.width}x${myBitmap.height}"
        initDurationTextView.text = "Init Duration: $initDurationInSeconds"
        detectionTypeTextView.text = "Detection Type: $detectionType"
        facesDetectedTextView.text = "Detected faces: $detectedFacesCount"
    }

}